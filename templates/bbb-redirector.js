const http = require('http');
const URL = require('url').URL;

const server = http.createServer((req, res) => {
  const url = new URL(req.url, 'https://x/')
  console.log(req.url)

  const options = {
    hostname: 'localhost',
    port: 8080,
    path: url.pathname+url.search
  }
  const bbb_req = http.request(options, (bbb_res) => {
    res.statusCode = bbb_res.statusCode
    for(let key in bbb_res.headers) {

        // replace location header
        if(key == 'location') {
            console.log('Catch redirect!')
            const targetUrl = new URL(bbb_res.headers['location'])
            targetUrl.hostname = req.headers.host
            console.log('new target: '+targetUrl.toString())
            res.setHeader('location', targetUrl.toString())
        } else {
            res.setHeader(key, bbb_res.headers[key])
        }
    }

    
    bbb_res.setEncoding('utf8');
    bbb_res.on('data', (chunk) => {
      res.write(chunk)
    });
    bbb_res.on('end', () => {
      res.end()
    });
  })
  bbb_req.end()

});
server.on('clientError', (err, socket) => {
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});
server.listen(process.env.PORT || 8087);
